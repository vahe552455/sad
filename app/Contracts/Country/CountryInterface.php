<?php

namespace App\Contracts\Country;

interface CountryInterface {
    public function all();
    public function countryJson();

}


class Auth {
    constructor () {
        this.token = window.localStorage.getItem('token');
        let userData = window.localStorage.getItem('user');
        this.user = userData ? JSON.parse(userData) : null;
        if (this.token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.token;
            this.getUser();
        }
             // console.log(axios.defaults.headers.common);
    }
    login (token, user) {
        window.localStorage.setItem('token', token);
        window.localStorage.setItem('user', JSON.stringify(user));
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        this.token = token;
        this.user = user;
        Event.$emit('userIsLoged');
    }
    check () {
        return !! this.token;
    }
    getUser() {
        api.call('get', '/api/get-user')
            .then(response => {
                Event.$emit('userIsLoged');
                this.user = response.data;
            }).catch((error)=>{
            Event.$emit('userIsLoged');

        });

    }
    logout() {
        var self = this;
        axios.post('api/logout').then(function (data) {
            window.localStorage.removeItem('token');
            window.localStorage.removeItem('user')  ;
            self.token  = null;
            self.user = null;
            Event.$emit('userIsLoged');
        }).catch((data)=> {
            window.localStorage.removeItem('token');
            window.localStorage.removeItem('user')  ;
            self.token  = null;
            self.user = null;
            Event.$emit('userIsLoged');
        }).then(function () {
            return true;
        })
    }
}
export default  Auth;


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Api from './api.js';
window.api = new Api();

import Auth from './auth.js';
window.auth = new Auth();

require('./vendor/js/sb-admin.js');
require('./../css/sb-admin.css')
import navbarComponent from './components/admin/NavbarComponent';
import leftbarComponent from './components/admin/LeftbarComponent';
import login from './components/admin/login/LoginComponent';
import register from './components/admin/register/RegisterComponent';
import user from './components/admin/users/Users';
import '@fortawesome/fontawesome-free/css/all.css';
import layout from './Layout.vue'
Vue.component('layout', layout);
Vue.component('navbar-component',navbarComponent);
Vue.component('leftbar-component',leftbarComponent);
Vue.component('login',login);
Vue.component('register',register);
import home from './components/Home'
import VueRouter from 'vue-router';

Vue.use(VueRouter);
window.Event = new Vue;
import App from './App.vue';
const routes = [

    {
        path:'/login',
        name:'login',
        component: login,
    },

    {
        path:'/home',
        name:'home',
        component: home,
        meta: {
            middlewareAuth:true
        },
    },

    {
        path:'/register' ,
        component: register,
    },

    {
        path:'/users' ,
        component: user,
        meta: {
            middlewareAuth:true,
            middlewareAdmin:true,
        },
    },
]

const router = new VueRouter({
    mode:'history',
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.middlewareAuth)) {
        if (!auth.check()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });
            return;
        }
    }
    if(to.matched.some(record => record.meta.middlewareAdmin))
    if(!auth.user.role) {
        next({path: '/home'})
        return;
    }
    next();
})


// Vue.component('example', require('./components/Example.vue'));

/*const app = new Vue({
    el: '#app'
});*//**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    render: h => h(App),
    router
});

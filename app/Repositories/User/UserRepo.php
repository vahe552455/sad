<?php

namespace App\Repositories\User;

use App\Contracts\User\UserInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserRepo implements UserInterface {

    protected $model;

    public function __construct(User $user)
    {
        $this->model =$user;
    }

    public function all() {

        return $this->model->where('role','!=',1)->with('country')->get();
    }

    public function show($id) {
        return $this->model->where('id', $id)->with('country')->first();
    }

    public function delete($id) {
        return   $this->model->find($id)->delete();
    }

    public function store($data) {
        $createData = $data->only(['name','email','country_id','password']);
        $createData['password'] = bcrypt($createData['password']);
        $user = $this->model->create($createData);
        return $user->load('country');
    }

    public function update($data , $id) {

        $updateData = $data->only(['email','name','country_id']);
        $data = $this->model->find($id);
        $data->update($updateData);
        return  $data->load('country');
    }

    public function login ($user) {
        $client = DB::table('oauth_clients')
            ->where('password_client', true)
            ->first();

        // Make sure a Password Client exists in the DB
        if (!$client) {
            return response()->json([
                'message' => 'Laravel Passport is not setup properly.',
                'status' => 500
            ], 500);
        }
        $data = [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => request(  'email'),
            'password' => request('password'),
        ];
        $request = Request::create('/oauth/token', 'POST', $data);
        $response = app()->handle($request);

        if ($response->getStatusCode() != 200) {
            return response()->json([
                'message    ' => 'Wrong email or password',
                'status' => 422
            ], 422);
        }
        $data = json_decode($response->getContent());
        return response()->json([
            'token' => $data->access_token,
            'user' => $user,
            'status' => 200
        ]);
    }
}


<?php

namespace App\Http\Controllers\Api\Admin\Users;

use App\Contracts\Country\CountryInterface;
use App\Http\Requests\UserChange;
use App\Http\Requests\UserValidate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\User\UserInterface;
class UserController extends Controller
{

    protected $userRepo;
    protected $countryRepo;
    public function __construct(UserInterface $users, CountryInterface $country)
    {
        $this->userRepo = $users;
        $this->countryRepo = $country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userRepo->all();
        return response()->json([
            'users' => $users,
            'status' => 200
        ], 200);
    }

    public function edit($id)
    {
        $countries = $this->countryRepo->all();
        $users = $this->userRepo->show($id);
        return response()->json(compact(['countries','users']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserChange $request)
    {
        $createdUser  = $this->userRepo->store($request);
        return response()->json([
            'user'=>$createdUser,
            'message'=>'User created successfully'
            ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserChange $request, $id)
    {
        $data =  $this->userRepo->update($request ,$id);
        return response()->json(['data'=>$data,'success'=>'Updated successfully'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $data =  $this->userRepo->delete($id);
       if($data) {
           return response()->json(['success'=>'User deleted successfully '],200);
       }
       return response()->json(['error'=>'Given data not founded'],404 );
    }
}

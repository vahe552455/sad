<?php

use Illuminate\Database\Seeder;
use App\Contracts\Country\CountryInterface;
use App\Models\Country;
class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(CountryInterface $country)
    {
        $countries = $country->countryJson();
        foreach ($countries as $country) {
            Country::create([
                'name'          => ((isset($country['name'])) ? $country['name'] : null),
                'currency'      => ((isset($country['currency'])) ? $country['currency'] : null),
                'capital'       => ((isset($country['capital'])) ? $country['capital'] : null),
                'currency_code' => ((isset($country['currency_code'])) ? $country['currency_code'] : null)
            ]);

        }
    }
}

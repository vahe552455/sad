<?php

namespace App\Contracts\User;

interface UserInterface {
    public function all();
    public function show($id);
    public function store($data);
    public function update($request, $id);
    public function login($user);
    public function delete($id);
}

<?php

namespace App\Repositories\Country;

use App\Contracts\Country\CountryInterface;
use App\Models\Country;

class CountryRepo implements CountryInterface {

    protected $model;

    public function __construct(Country $country)
    {
        $this->model = $country;
    }

    public function all() {
        return $this->model->all();
    }
    public function countryJson()
    {
        $data = database_path('/countries/countries.json');
        return json_decode(file_get_contents($data),true);
    }
}


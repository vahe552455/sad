<?php

namespace App\Http\Controllers;

use App\Contracts\Country\CountryInterface;
use App\Contracts\User\UserInterface;
use App\Http\Requests\UserCreate;
use App\Http\Requests\UserValidate;
use http\Env\Response;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class AuthController extends Controller
{
    protected $countryRepo;
    protected $userRepo;
    public function __construct(CountryInterface $country, UserInterface $user)
    {
        $this->countryRepo = $country;
        $this->userRepo = $user;
    }

    public function register(UserCreate $request)
    {
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'country_id' =>$request->get('country')
        ]);
       return $this->userRepo->login($user);
    }

    public function countries() {
        return $this->countryRepo->all();
    }


    public function login(UserValidate $request)
    {
        $user = User::whereEmail($request->email)->first();
        if (!$user) {
            return response()->json([
                'message' => 'Wrong email',
            ],422);
        }
        if (!Hash::check($request->password, $user->password)) {
            return response()->json([
                'message' => 'Wrong password',
            ], 422);
        }

        return $this->userRepo->login($user);

    }

    public function logout() {
        $accessToken = auth()->user()->token();
        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);
        $accessToken->revoke();
        return response()->json(['status' => 200]);
    }

    public function getUser() {
         return  response()->json(auth()->user(),200);
    }

    protected function respondWithToken($token,$user)
    {
        return response()->json([
            'user'=>$user,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
